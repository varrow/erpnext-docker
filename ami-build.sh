#!/bin/bash

yum update -y
yum install wget curl amazon-efs-utils -y
amazon-linux-extras install docker -y
usermod -a -G docker ec2-user
systemctl enable --now docker.service
curl -L "https://github.com/docker/compose/releases/download/1.29.2/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
chmod +x /usr/local/bin/docker-compose

dd if=/dev/zero of=/swapfile bs=1M count=1024
chmod 600 /swapfile
mkswap /swapfile
echo "/swapfile swap swap defaults 0 0" >> /etc/fstab
swapon -a

runuser - ec2-user -c "/home/ec2-user/erpnext-docker/pre.sh -e aws -w /home/ec2-user/erpnext-docker -d dummy -p dummy -u dummy -a dummy"
runuser - ec2-user -c "docker-compose -f /home/ec2-user/erpnext-docker/docker-compose.yml pull"

rm -rf /home/ec2-user/erpnext-docker
